# see https://stackoverflow.com/questions/47913041/initial-setup-of-terraform-backend-using-terraform

provider "aws" {
  region = "eu-central-1"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "guggenberger.rocks.lonewolf"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

//resource "aws_dynamodb_table" "terraform_state_lock" {
//  name           = "app-state"
//  read_capacity  = 1
//  write_capacity = 1
//  hash_key       = "LockID"
//
//  attribute {
//    name = "LockID"
//    type = "S"
//  }
//}