from PIL import Image
from resizeimage import resizeimage
import os
import shutil

result_path = "../src/assets/icon"


def resize(name, width):
    with open('pwa_icon.png', 'r+b') as f:
        with Image.open(f) as image:
            cover = resizeimage.resize_cover(image, [width, width])
            cover.save(
                f"{result_path}/{name}{width}x{width}.png", image.format)


platforms = [
    ['android-icon-', [144, 192, 36, 48, 72, 96]],
    ['apple-icon-', [114, 120, 144, 152, 180, 57, 60, 72, 76]],
    ['ms-icon-', [144, 150, 310, 70]],
    ['favicon-', [16, 32, 96]]
]


# Do the bunch of sizes
for platform in platforms:
    for size in platform[1]:
        resize(platform[0], size)


# Copy some special named files
shutil.copyfile(f"{result_path}/android-icon-192x192.png",
                f"{result_path}/apple-icon-precomposed.png")
shutil.copyfile(f"{result_path}/android-icon-192x192.png",
                f"{result_path}/apple-icon.png")


# The .ico file also needs special handling
with open('pwa_icon.png', 'r+b') as f:
    with Image.open(f) as img:
        res = resizeimage.resize_cover(img, [16, 16])
        res.save(f"{result_path}/favicon.ico", "ico")


# This is the file Android actually uses as an app icon
with open('pwa_icon.png', 'r+b') as f:
    with Image.open(f) as img:
        res = resizeimage.resize_cover(img, [512, 512])
        res.save(f"{result_path}/logo.png", img.format)
