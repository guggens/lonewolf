# Generate all the PWA icons

This small Python script generates all necessary icon sizes for thw PWA. You can use [Pipenv](https://pipenv.readthedocs.io/en/latest/) to run it:

- pipenv install
- pipenv shell
- python resize.py

The images will be placed into an output folder.
