output "address" {
  value = "${aws_elb.web.dns_name}"
}

output "ec2-instance-public-ip" {
  value = "${aws_eip.ip-web.public_ip}"
}