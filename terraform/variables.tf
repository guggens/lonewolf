variable "public_key_path" {
  default = "~/.ssh/id_rsa.pub"
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.
Example: ~/.ssh/id_ed25519.pub
DESCRIPTION
}

variable "key_name" {
  default = "guggens@gmail.com ssh key"
  description = "Desired name of AWS key pair"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "eu-central-1"
}

# Ubuntu Precise 18.04 LTS (HVM), SSD Volume Type
variable "aws_amis" {
  type = "map"
  default = {
    eu-central-1 = "ami-090f10efc254eaf55"
  }
}