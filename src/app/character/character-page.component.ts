import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-character',
  templateUrl: 'character-page.component.html',
  styleUrls: ['character-page.component.scss']
})
export class CharacterPage {

  character: Character = new Character()
  random: number = 0

  constructor(private storage: Storage) {
    storage.get('character').then((character) => {
      if (character != null) {
        this.character = character
      }
    });
    this.newRandomNumber()
  }

  newRandomNumber() {
    this.random = Math.floor(Math.random() * (10));
  }

  save() {
    this.storage.set('character', this.character)
  }

  clearPath() {
    this.storage.remove('path')
  }

  clearCharacter() {
    this.storage.remove('character')
  }

}

class Character {

  combatSkill: number = 10
  combatSkillBonus: number = 0
  endurance: number = 20
  enduranceBonus: number = 0
  currentEndurance: number = 20
  gold: number = 0
  meals: number = 0
  weapons: string = ""
  backpack: string = ""
  special: string = ""

}