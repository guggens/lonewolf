import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'library',
        children: [
          {
            path: '',
            loadChildren: '../library/library.module#LibraryPageModule'
          }
        ]
      },
      {
        path: 'read',
        children: [
          {
            path: '',
            loadChildren: '../read/read.module#ReadPageModule'
          }
        ]
      },
      {
        path: 'character',
        children: [
          {
            path: '',
            loadChildren: '../character/character.module#CharacterPageModule'
          }
        ]
      },
      {
        path: 'fight',
        children: [
          {
            path: '',
            loadChildren: '../fight/fight.module#FightPageModule'
          }
        ]
      },
      {
        path: 'map',
        children: [
          {
            path: '',
            loadChildren: '../map/map.module#MapPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/read',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/library',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
