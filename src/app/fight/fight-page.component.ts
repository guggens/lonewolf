import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-fight',
  templateUrl: 'fight-page.component.html',
  styleUrls: ['fight-page.component.scss']
})
export class FightPage {

  loneWolfEndurance: number = 20
  currentLoneWolfEndurance: number = 20
  loneWolfCombatSkill: number = 10

  enemyEndurance: number = 20
  currentEnemyEndurance: number = 20
  enemyCombatSkill: number = 10

  fightRounds: FightRound[] = []

  fightRound() {
    if (this.fightRounds.length == 0) {
      this.currentEnemyEndurance = this.enemyEndurance
      this.currentLoneWolfEndurance = this.loneWolfEndurance
    }

    let round = new FightRound()
    round.random = Math.floor(Math.random() * (10));
    round.loneWolfEndurance = this.currentLoneWolfEndurance
    round.enemyEndurance = this.currentEnemyEndurance
    round.computeLoss(this.loneWolfCombatSkill-this.enemyCombatSkill)

    this.currentLoneWolfEndurance += round.loneWolfLoss
    this.currentEnemyEndurance += round.enemyLoss

    if (this.currentEnemyEndurance < 0) {
      this.currentEnemyEndurance = 0
    }
    if (this.currentLoneWolfEndurance < 0) {
      this.currentLoneWolfEndurance = 0
    }

    this.fightRounds.push(round)
  }

  clear() {
    this.fightRounds = []
    this.currentEnemyEndurance = this.enemyEndurance
    this.currentLoneWolfEndurance = this.loneWolfEndurance
  }

}

class FightRound {
  random: number = 0;
  loneWolfLoss: number = 0;
  enemyLoss: number = 0;
  loneWolfEndurance: number = 0;
  enemyEndurance: number = 0;

  computeLoss(ratio: number) {

    if (ratio < -11) {
      ratio = -11
    }

    if (ratio > 11) {
      ratio = 11
    }

    switch (ratio) {
      case -11: {
        let enemy = [0,0,0,0,-1,-2,-3,-4,-5,-6]
        let loneWolf = [this.loneWolfEndurance,this.loneWolfEndurance,-8,-8,-7,-6,-5,-4,-3,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case -10:
      case -9: {
        let enemy = [0,0,0,-1,-2,-3,-4,-5,-6,-7]
        let loneWolf = [this.loneWolfEndurance,-8,-7,-7,-6,-6,-5,-4,-3,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case -8:
      case -7: {
        let enemy = [0,0,-1,-2,-3,-4,-5,-6,-7,-8]
        let loneWolf = [-8,-7,-6,-6,-5,-5,-4,-3,-2, 0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case -6:
      case -5: {
        let enemy = [0,-1,-2,-3,-4,-5,-6,-7,-8,-9]
        let loneWolf = [-6,-6,-5,-5,-4,-4,-3,-2,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case -4:
      case -3: {
        let enemy = [-1,-2,-3,-4,-5,-6,-7,-8,-9,-10]
        let loneWolf = [-6,-5,-5,-4,-4,-3,-2,-1,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case -2:
      case -1: {
        let enemy = [-2,-3,-4,-5,-6,-7,-8,-9,-10,-11]
        let loneWolf = [-5,-5,-4,-4,-3,-2,-2,-1,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case 0: {
        let enemy = [-3,-4,-5,-6,-7,-8,-9,-10,-11,-12]
        let loneWolf = [-5,-4,-4,-3,-2,-2,-1,0,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case 1:
      case 2:  {
        let enemy = [-4,-5,-6,-7,-8,-9,-10,-11,-12,-14]
        let loneWolf = [-5,-4,-3,-3,-2,-2,-1,0,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case 3:
      case 4:  {
        let enemy = [-5,-6,-7,-8,-9,-10,-11,-12,-14,-16]
        let loneWolf = [-4,-3,-3,-2,-2,-2,-1,0,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case 5:
      case 6:  {
        let enemy = [-6,-7,-8,-9,-10,-11,-12,-14,-16,-18]
        let loneWolf = [-4,-3,-3,-2,-2,-1,0,0,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case 7:
      case 8:  {
        let enemy = [-7,-8,-9,-10,-11,-12,-14,-16,-18, this.enemyEndurance]
        let loneWolf = [-4,-3,-2,-2,-2,-1,0,0,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case 9:
      case 10:  {
        let enemy = [-8,-9,-10,-11,-12,-14,-16,-18, this.enemyEndurance, this.enemyEndurance]
        let loneWolf = [-3,-3,-2,-2,-2,-1,0,0,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }
      case 11:  {
        let enemy = [-9,-10,-11,-12,-14,-16,-18, this.enemyEndurance, this.enemyEndurance, this.enemyEndurance]
        let loneWolf = [-3,-2,-2,-2,-1,-1,0,0,0,0]
        this.loneWolfLoss = loneWolf[this.random]
        this.enemyLoss = enemy[this.random]
        break;
      }

    }


  }


}

