import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import {Observable} from "rxjs";
import * as xpath from "xpath";
import {DOMParser} from 'xmldom';
import { Storage } from '@ionic/storage';

@Injectable()
export class AonProvider {

    book = null

    xmlDomTree = undefined

    constructor(public http: HttpClient, public storage: Storage) {
        this.storage.get("book").then((selectedBook) => {
            if (selectedBook == null) {
                this.selectBook("01fftd")
            } else {
                this.book = selectedBook
            }
        })
    }

    public getMapUrl(): string {
        return "https://www.projectaon.org/data/trunk/en/jpeg/lw/" + this.book + "/ill/chalk/map.jpg"
    }

    public selectBook(book: string) {
        this.book = book
        this.storage.remove("path")
        this.storage.set("book", book)
        this.xmlDomTree = undefined
    }

    public getNumberedSection(sectionNumber: number): Observable<string> {
        return new Observable<string>(
            (observer) => {
                this.getXmlDomTree().subscribe(
                    (doc: Node) => {
                        var section = xpath.evaluate( "/gamebook/section[@id='title']/data/section[@id='numbered']/data/section[@id='sect" + sectionNumber + "']/data", doc, null, XPathResult.ANY_TYPE, null ) as any;
                        if (section.nodes[0] == undefined) {
                            section = xpath.evaluate( "/gamebook/section[@id='title']/data/section[@id='part1']/data/section[@id='sect" + sectionNumber + "']/data", doc, null, XPathResult.ANY_TYPE, null ) as any;
                            if (section.nodes[0] == undefined) {
                                section = xpath.evaluate( "/gamebook/section[@id='title']/data/section[@id='part2']/data/section[@id='sect" + sectionNumber + "']/data", doc, null, XPathResult.ANY_TYPE, null ) as any;
                            }
                        }
                        observer.next(section.nodes[0])
                        observer.complete()
                    }
                )
            }
        )
    }

    public getSection(sectionName: string): Observable<string> {
        return new Observable<string>(
            (observer) => {
                this.getXmlDomTree().subscribe(
                    (doc: Node) => {
                        var section = xpath.evaluate( "//section[@id='" + sectionName + "']", doc, null, XPathResult.ANY_TYPE, null ) as any;

                        observer.next(section.nodes[0])
                        observer.complete()
                    }
                )
            }
        )
    }

    public getImageRef(shortname: string): string {
        if (location.hostname === "localhost") {
            return `http://www.projectaon.org/data/trunk/en/png/lw/${this.book}/ill/chalk/` + shortname;
        }
        return `/aon/png/lw/${this.book}/ill/chalk/` + shortname;
    }

    public count(doc: Node, xpathExpression: string) {
        return xpath.evaluate("count(" + xpathExpression + ")", doc, null, XPathResult.NUMBER_TYPE, null).numberValue
    }

    private getXmlDomTree(): Observable<Node> {
        return new Observable<Node>(
            (observer) => {
                if (this.xmlDomTree == undefined) {

                    let url = ""

                    if (location.hostname === "localhost" ) {
                        url = `http://lonewolf-elb-1424212260.eu-central-1.elb.amazonaws.com/aon/xml/${this.book}.xml`
                    } else {
                        url = `/aon/xml/${this.book}.xml`
                    }


                    this.http.get(url, {responseType: 'text'}).subscribe(
                        (data: string) => {

                            data = data.replace(/<ch.eacute\/>/g, 'é')
                            data = data.replace(/<ch.endash\/>/g, '-')
                            data = data.replace(/<ch.emdash\/>/g, '—')
                            data = data.replace(/<ch.copy\/>/g, '©')
                            data = data.replace(/<ch.ouml\/>/g, 'ö')
                            data = data.replace(/<ch.aacute\/>/g, 'á')
                            data = data.replace(/<ch.apos\/>/g, '\'')
                            data = data.replace(/<ch.ellips\/>/g, '…')
                            data = data.replace(/<ch.plus\/>/g, '+')
                            data = data.replace(/<ch.minus\/>/g, '−')
                            data = data.replace(/<quote>/g, '„')
                            data = data.replace(/<\/quote>/g, '‟')
                            data = data.replace(/&inclusion.joe.dever.endowment;/g, '')

                            var doc = new DOMParser().parseFromString(data, "text/xml")

                            this.xmlDomTree = doc
                            observer.next(doc)
                            observer.complete()
                        }
                    )
                } else {
                    observer.next(this.xmlDomTree)
                    observer.complete()
                }
            }
        )
    }

}
