import {Component, ViewChild} from '@angular/core';
import * as xpath from 'xpath';
import {HttpClient} from "@angular/common/http";
import { AonProvider } from "../providers/aon"
import { Storage } from '@ionic/storage';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-read',
  templateUrl: 'read-page.component.html',
  styleUrls: ['read-page.component.scss']
})
export class ReadPage {
    @ViewChild('page') pageTop: IonContent;

    sectionNumber:number = null
    path: number[] = []
    section: any
    nextSection: string = ""
    sectionTitle: string = ""
    nodes: Tag[] = []
    puzzleResult: number = null

    constructor(public http: HttpClient, public aon: AonProvider, public storage: Storage) {
        this.ionViewWillEnter()
    }

    ionViewWillEnter()  {
        this.storage.get('path').then((path) => {
            if (path != null && path.length > 1) {
                this.path = path;
                var lastTurn = this.path.pop()
                this.turnTo(lastTurn)
            } else {
                this.path = []
                this.sectionNumber = null
                this.gotoSection("dedicate")
            }
        });
    }

    private gotoSection(name: string) {
        if (name == "sect1") {
            this.turnTo(1)
        }
        this.aon.getSection(name).subscribe(
            (data: any) => {

                let anySection = xpath.evaluate("data", data, null, XPathResult.ANY_TYPE, null) as any

                this.sectionTitle = xpath.evaluate("meta/title", data, null, XPathResult.STRING_TYPE, null).stringValue

                if (name == "gamerulz") {
                    let rulesSection = anySection.nodes[0].cloneNode(true)
                    let subsections = rulesSection.getElementsByTagName("section")
                    let sectionCount = subsections.length //subsections.lenght changes when removing, so cache value!!
                    for (let i = 0; i < sectionCount; i++) {
                        rulesSection.removeChild(subsections[i])
                    }
                    this.section = rulesSection
                } else {
                    this.section = anySection.nodes[0].childNodes
                }

                this.nextSection = xpath.evaluate("meta/link[@class='next']/@idref", data, null, XPathResult.STRING_TYPE, null).stringValue

                this.pageTop.scrollToTop();
            }
        )

    }


    private xpathString(expression: string, node: any) : string {
        return xpath.evaluate( expression, node, null, XPathResult.STRING_TYPE, null ).stringValue;
    }

    public turnTo(sectionNumber:number) {

        this.sectionNumber = sectionNumber
        this.path.push(sectionNumber)
        this.storage.set("path", this.path)
        this.aon.getNumberedSection(sectionNumber).subscribe(
            (data: any) => {
                this.nodes = []
                for (let i = 0; i < data.childNodes.length; i++) {
                    let childNode = this.parseNode(data.childNodes[i])
                    if (childNode) {
                        this.nodes.push(childNode)
                    }
                }
            }
        )
    }

    private parseNode(node: any) : Tag {
        let tag = new Tag(node.tagName)

        if (tag.type == "p") {
            // <p>As you approach, the group of people stop talking. You can see by their expressions that they recognize your green Kai cloak. Slowly, one of the men extends his hand in friendship and says, <quote>My Lord, we had heard a rumour that the Kai were destroyed. Heaven be praised that it is not so. We feared all was lost.</quote></p>
            for (let i = 0; i < node.childNodes.length; i++) {
                let childNode = node.childNodes[i]
                if (childNode.childNodes) {


                    if (childNode.childNodes[0]) {
                        tag.text += childNode.childNodes[0].nodeValue
                    }
                } else {
                    tag.text += childNode.nodeValue
                }
            }
        }
        if (tag.type == "poetry") {
            // <poetry>
            //     <line>When the full moon shines o<ch.apos/>er the temple deep,</line>
            // <line>A sacrifice will stir from sleep</line>
            // <line>The legions of a long forgotten lord.</line>
            // <line>When a fair royal maid on the altar dies,</line>
            // <line>The dead of Maakengorge shall rise</line>
            // <line>To claim their long-awaited reward.</line>
            // </poetry>
            for (let i = 0; i < node.childNodes.length; i++) {
                let childNode = node.childNodes[i]
                if (childNode.childNodes) {
                    if (childNode.childNodes[0]) {
                        tag.text += childNode.childNodes[0].nodeValue + "<br/>"
                    }
                } else {
                    tag.text += childNode.nodeValue
                }
            }
        }
        if (tag.type == "choice") {
            //<choice idref="sect141">If you wish to use your Kai Discipline of Sixth Sense, <link-text>turn to 141</link-text>.</choice>

            tag.turnId = +this.xpathString("@idref", node).substr(4)

            for (let i = 0; i < node.childNodes.length; i++) {
                let childNode = node.childNodes[i]
                if (childNode.tagName == "link-text") {
                    tag.turnText = this.xpathString( "text()", childNode).trim();
                } else {
                    let addedText = ""
                    if (childNode.childNodes) {
                        addedText = childNode.childNodes[0].nodeValue
                    } else {
                        addedText = childNode.nodeValue
                    }

                    if (tag.turnText == null) {
                        tag.turnPrefix += addedText;
                    } else {
                        tag.turnPostfix += addedText;
                    }
                }
            }
        }
        if (tag.type == "ul") {
            // <ul>
            //     <li>Sword<ch.emdash/>4 Gold Crowns</li>
            // <li>Dagger<ch.emdash/>2 Gold Crowns</li>
            // <li>Short Sword<ch.emdash/>3 Gold Crowns</li>
            // <li>Warhammer<ch.emdash/>6 Gold Crowns</li>
            // <li>Spear<ch.emdash/>5 Gold Crowns</li>
            // <li>Mace<ch.emdash/>4 Gold Crowns</li>
            // <li>Fur Blanket<ch.emdash/>3 Gold Crowns</li>
            // <li>Backpack<ch.emdash/>1 Gold Crown<footref id="sect181-1" idref="sect181-1-foot"/></li>
            //     </ul>
            tag.text = node.childNodes
        }
        if (tag.type == "illustration") {
            // <illustration class="float">
            //     <meta>
            //         <creator>Gary Chalk</creator>
            // <description>A young theurgist of the Magicians<ch.apos/> Guild of Toran.</description>
            // </meta>
            // <instance class="html" src="ill9.png" width="386" height="676"  mime-type="image/png"/>
            // <instance class="html-compatible" src="ill9.gif" width="386" height="676"  mime-type="image/gif"/>
            // <instance class="pdf" src="ill9.pdf" width="386" height="676" />
            //     </illustration>
            //alvarez images are not hosted anymore
            if (this.xpathString("meta/creator/text()", node).toLowerCase().indexOf("alvarez") != -1) {
                return null;
            }
            tag.text = this.xpathString("meta/description/text()", node)
            tag.imageFile = this.aon.getImageRef(this.xpathString("instance[1]/@src", node))
        }
        if (tag.type == "combat") {
            // <combat><enemy>Bodyguard</enemy><enemy-attribute class="combatskill">11</enemy-attribute><enemy-attribute class="endurance">21</enemy-attribute></combat>
            tag.text = this.xpathString("enemy/text()", node)
            tag.combatskill = Number(this.xpathString("enemy-attribute[@class='combatskill']/text()", node))
            tag.endurance = Number(this.xpathString("enemy-attribute[@class='endurance']/text()", node))
        }
        if (tag.type == "deadend") {
            // <deadend>As the world darkens, the last thing you feel is the black shafts of their arrows deep in your chest. You have failed in your mission.</deadend>
            tag.text = this.xpathString( "text()", node)
        }

        if (tag.type == "puzzle") {
            // <puzzle idref="sect373">When you have broken the code, write the numbers in order and <choose test="has-numbered-section-list"><when value="true"><a idref="part2">turn to the entry number</a></when><otherwise>turn to the entry number</otherwise></choose> that they indicate.<footref id="sect331-1" idref="sect331-1-foot" /></puzzle>
            tag.text = this.xpathString( "text()", node)
        }



        return tag;
    }

    private back() {
        if (this.path != null && this.path.length > 1) {
            this.path.pop()
            this.turnTo(this.path.pop())
        }
    }


//TODO: Blockquote
// <illustration class="float">
//         <meta>
//             <creator>Gary Chalk</creator>
// <description>Opening the bag, you find a message.</description>
// </meta>
// <instance class="html" src="ill15.png" width="386" height="648"  mime-type="image/png"/>
//     <instance class="html-compatible" src="ill15.gif" width="386" height="648"  mime-type="image/gif"/>
//     <instance class="pdf" src="ill15.pdf" width="386" height="648" />
//     <instance class="text">
//         <blockquote>
//             <p><foreign xml:lang="x-giak"><quote>Orgadak shada taag okak<ch.emdash/>orgadak ok<ch.emdash/>nara ek ash jek eg Helgedad.</quote></foreign></p>
// </blockquote>
// </instance>
// </illustration>

//TODO: Footnote
// <footnotes>
// <footnote id="sect349-1-foot" idref="sect349-1">
//         <p>Later books assume that you keep the Crystal Star Pendant and ask you if you possess it to see if you<ch.apos/>ve ever met Banedon. It also may or may not be useful in other situations. You may decide whether or not to keep the Pendant.</p>
// </footnote>
// </footnotes>

}

export class Tag {
    type: string
    constructor(type: string) {
        this.type = type
    }

    text: string = ""
    turnId: number = null
    turnPrefix:string = ""
    turnText:string = null
    turnPostfix:string = ""
    combatskill: number = null
    endurance: number = null
    imageFile: string = null
}
