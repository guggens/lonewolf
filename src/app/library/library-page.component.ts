import { Component } from '@angular/core';
import * as xpath from 'xpath';
import {HttpClient} from "@angular/common/http";
import { AonProvider } from "../providers/aon"
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-map',
  templateUrl: 'library-page.component.html',
  styleUrls: ['library-page.component.scss']
})
export class LibraryPage {

  books: Book[] = []

  constructor(public aon: AonProvider, public storage: Storage) {
    this.books.push(new Book("Flight from the Dark", "01fftd", 1))
    this.books.push(new Book("Fire on the Water", "02fotw", 2))
    this.books.push(new Book("The Caverns of Kalte", "03tcok", 3))
    this.books.push(new Book("The Chasm of Doom", "04tcod", 4))
    this.books.push(new Book("Shadow on the Sand", "05sots", 5))
    this.books.push(new Book("The Kingdoms of Terror", "06tkot", 6))
    this.books.push(new Book("Castle Death", "07cd", 7))
    this.books.push(new Book("The Jungle of Horrors", "08tjoh", 8))
    this.books.push(new Book("The Claudron of Fear", "09tcof", 9))
    this.books.push(new Book("The Dungeons of Torgar", "10tdot", 10))
    this.books.push(new Book("The Prisoners of Time", "11tpot", 11))
    this.books.push(new Book("The Masters of Darkness", "12tmod", 12))
  }

  selectBook(book: string) {
    this.aon.selectBook(book)
  }
}

class Book {

  constructor(title: string, id: string, number: number) {
    this.title = title;
    this.id = id;
    this.number = number;
  }

  title: string
  id: string
  number: number
}
