import { Component } from '@angular/core';
import * as xpath from 'xpath';
import {HttpClient} from "@angular/common/http";
import { AonProvider } from "../providers/aon"

@Component({
  selector: 'app-map',
  templateUrl: 'map-page.component.html',
  styleUrls: ['map-page.component.scss']
})
export class MapPage {
  mapUrl: string

  constructor(public aon: AonProvider) {
  }

  ionViewWillEnter()  {
    this.mapUrl = this.aon.getMapUrl()
  }
}